﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    interface IZone
    {
        string Name { get; }

        void AddNeighbor(IZone neighborZone);
        void AddEnterAction(IExecutable action);
        void AddContent(IInteractable inter);
        IExecutable GetContentExecutable();
        void SetAsPlayerStartZone();
    }
}
