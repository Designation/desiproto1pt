﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;

namespace DesiProto1PT
{
    class GameWorldManager
    {
        // creates and fills all zones of the game world

        // used for save-load
        private List<IZone> zoneList = new List<IZone>();
        private IZone startingZone;

        internal void Init()
        {
            EventAggregator.GetInstance().Subscribe(this);
            this.GenerateWorld_Debug();
            this.PrepareStartingZone();
        }

        private void PrepareStartingZone()
        {
            this.startingZone.SetAsPlayerStartZone();
        }

        private void GenerateWorld_Debug()
        {
            var place = new Zone("debug place", "shortDesc", "longDesc");
            this.startingZone = place;

            var picture_Action = new PrintOptionsCommand("picture");
            var picture = new Interactable("picture", picture_Action);
            place.AddContent(picture);
            picture_Action.AddOption("look at it", new PrintCommand("you looked at it"));
            picture_Action.AddOption("back", new RemoveExecutionCommand());
        }

        private void GenerateWorld()
        {
            //todo
        }
    }
}