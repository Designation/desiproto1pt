﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class Player
    {
        // contains of the info related to the player: HP, current zone, money, inventory, etc.

        public int CurrentHitpoints { get; set; }
        public int CurrentMoney { get; set; }
        public string CurrentObjective { get; set; }
        public List<IItem> Inventory { get; set; }
        public IZone CurrentZone { get; set; }

        private int maximumHitpoints;

        internal void Init()
        {
           EventAggregator.GetInstance().Subscribe(this);
            this.CurrentObjective = "Kauf Milch";
            this.maximumHitpoints = 100;
            this.CurrentHitpoints = 100;
            this.CurrentMoney = 5;
        }
    }
}
