﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    interface IInteractable
    {
        string Name { get; }

        void StartInteraction();
        void SetApproachAction(IExecutable approachAction);
    }
}
