﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class TextAdventureGame
    {
        // MAIN GAME CLASS
        // manages / contains / connects all game-modules

        // modules -> SOLID - S: Single responsibility principle
        public Player Player { get; set; }
        public GameWorldManager WorldManager { get; set; }
        public RandomEventManager EventManager { get; set; }
        public CommandStackManager CommandManager { get; set; }
        public EventAggregator EventAggregator { get; set; }

        static void Main(string[] args)
        {
            var game = new TextAdventureGame();
            game.Init();
            game.Start();
        }

        public void Init()
        {
            this.WorldManager = new GameWorldManager();
            this.Player = new Player();
            this.EventManager = new RandomEventManager();
            this.CommandManager = new CommandStackManager();
            this.EventAggregator = EventAggregator.GetInstance();

            this.CommandManager.Init();
            this.Player.Init();
            this.EventManager.Init();
            this.WorldManager.Init();
            this.EventAggregator.Init();
        }

        public void Start()
        {
            // intro
            String introText = "Es ist der Tag des G20 Gipfels in Hamburg und die Menschen sind schon in Aufruhr.\n";
            introText += "Als du dir eine Schüssel köstlicher Cornflakes zubereiten möchtest fällt dir auf, dass die Milch alle ist!\n";
            introText += "Du musst also hinaus in die vor wütenden Menschen nur so tobende Stadt um eine Packung Milch zu kaufen.\n";
            introText += "Für deine köstlichen Cornflakes!\n";
            new PrintCommand(introText).Execute();

            // actual game
            this.CommandManager.StartExecuting();

            // game END
            new PrintCommand("---- THE END ----").Execute();
            new PrintCommand("Thanks for playing. Press Enter to close this window.").Execute();

            // prevent window from closing until you press <Enter>
            Console.Read();
        }
    }
}
