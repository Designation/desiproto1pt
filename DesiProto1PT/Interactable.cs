﻿using DesiProto1PT.CommandPattern;
using System;

namespace DesiProto1PT
{
    internal class Interactable : IInteractable
    {
        public string Name { get; }

        private IExecutable interaction;
        private IExecutable approachAction;

        public Interactable(string name, IExecutable interaction)
        {
            this.Name = name;
            this.interaction = interaction;
        }

        public void StartInteraction()
        {
            if (this.approachAction != null)
            {
                this.approachAction.Execute();
            }

            this.interaction.AddToCommandStack();
        }

        public void SetApproachAction(IExecutable approachAction)
        {
            this.approachAction = approachAction;
        }
    }
}