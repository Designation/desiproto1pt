﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class CommandStackManager
    {
        // executes the actual game actions

        private Stack<IExecutable> commandStack;

        internal void Init()
        {
            EventAggregator.GetInstance().Subscribe(this);
            this.commandStack = new Stack<IExecutable>();
        }

        public void AddCommand(IExecutable command)
        {
            this.commandStack.Push(command);
        }

        private void ExecuteNextCommand()
        {
            this.commandStack.Peek().Execute();
        }

        public void StartExecuting()
        {
            while (this.commandStack.Count > 0)
            {
                this.ExecuteNextCommand();
            }
        }

        internal void RemoveLastCommand()
        {
            this.commandStack.Pop();
        }

        internal void ClearCommandStack()
        {
            this.commandStack.Clear();
        }
    }
}
