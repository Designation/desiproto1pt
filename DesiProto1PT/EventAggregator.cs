﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class EventAggregator
    {
        // manages the communication between the modules
        // <-- eventHandling -->

        private static EventAggregator eaInstance;

        private GameWorldManager worldManager;
        private CommandStackManager commandManager;
        private RandomEventManager randomEventManager;
        private Player player;

        private EventAggregator()
        {
        }

        // Singleton
        public static EventAggregator GetInstance()
        {
            if (EventAggregator.eaInstance == null)
            {
                EventAggregator.eaInstance = new EventAggregator();
            }

            return EventAggregator.eaInstance;
        }

        internal void Init()
        {
            // ...
        }

        internal string GetObjective()
        {
            return this.player.CurrentObjective;
        }

        public void AddToCommandStack(IExecutable command)
        {
            this.commandManager.AddCommand(command);
        }

        internal void SetPlayerStartZone(IZone zone)
        {
            this.player.CurrentZone = zone;
            this.commandManager.AddCommand(zone.GetContentExecutable());
        }

        public void RemoveLastCommand()
        {
            this.commandManager.RemoveLastCommand();
        }

        // SUBSCRIBE OF DIFFERENT TYPES

        internal void Subscribe(GameWorldManager gameWorldManager)
        {
            this.worldManager = gameWorldManager;
        }

        internal void Subscribe(CommandStackManager commandStackManager)
        {
            this.commandManager = commandStackManager;
        }

        internal void Subscribe(RandomEventManager randomEventManager)
        {
            this.randomEventManager = randomEventManager;
        }

        internal void Subscribe(Player player)
        {
            this.player = player;
        }

        internal void ClearCommandStack()
        {
            this.commandManager.ClearCommandStack();
        }
    }
}
