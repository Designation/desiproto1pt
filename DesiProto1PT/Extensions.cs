﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    static class Extensions
    {
        public static void AddToCommandStack(this IExecutable executable)
        {
            EventAggregator.GetInstance().AddToCommandStack(executable);
        }

    }
}
