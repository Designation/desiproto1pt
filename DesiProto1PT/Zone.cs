﻿using DesiProto1PT.CommandPattern;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class Zone : IZone
    {
        public string Name { get; }
        public List<IInteractable> InteractableList { get; set; } = new List<IInteractable>();
        public List<IZone> NeighborZoneList { get; set; } = new List<IZone>();

        public Zone(string name, string shortDesc, string longDesc)
        {
            this.Name = name;
        }

        public void AddNeighbor(IZone neighborZone)
        {
            this.NeighborZoneList.Add(neighborZone);
        }

        public void AddEnterAction(IExecutable action)
        {
            throw new NotImplementedException();
        }

        public void AddContent(IInteractable inter)
        {
            this.InteractableList.Add(inter);
        }

        public IExecutable GetContentExecutable()
        {
            // returns a PrintOptionsCommand, containing all options the player has in this zone
            // (all interaction possibilities and their IExecutable action if interacted with)

            var result = new PrintOptionsCommand(this.Name);

            foreach (var inter in this.InteractableList)
            {
                result.AddOption(inter.Name, new StartInteractionCommand(inter));
            }

            foreach (var zone in this.NeighborZoneList)
            {
                result.AddOption("go to " + zone.Name, new EnterZoneCommand(zone));
            }

            result.AddOption("look around", new PrintCommand("zone descrption"));
            result.AddOption("open menu", new OpenMenuCommand());

            return result;
        }

        public void SetAsPlayerStartZone()
        {
            EventAggregator.GetInstance().SetPlayerStartZone(this);
        }
    }
}
