﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    class OpenMenuCommand : IExecutable
    {

        public void Execute()
        {
            var result = new PrintOptionsCommand("menü");

            result.AddOption("Inventar", new PrintCommand("Inventar momentan nicht verfügbar"));
            result.AddOption("Aufgabe", new PrintCommand(EventAggregator.GetInstance().GetObjective()));
            result.AddOption("Spiel Beenden", new ClearCommandStackCommand());

            result.AddToCommandStack();
        }
    }
}
