﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    class StartInteractionCommand : IExecutable
    {
        private IInteractable interactable;

        public StartInteractionCommand(IInteractable interactable)
        {
            this.interactable = interactable;
        }

        public void Execute()
        {
            this.interactable.StartInteraction();
        }
    }
}
