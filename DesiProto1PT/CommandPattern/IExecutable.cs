﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    interface IExecutable
    {
        void Execute();
    }

    // ==== COMMAND PATTERN PLAN ====
    // - replace all Runnable with IAction[]
    // - IAction:
    // -> new APrint(String msg);
    // -> new AUpdateGoal(String newGoal);
    // -> new AEndInteraction(IInteractable inter);
    // -> new ARemoveFromZone(IInteractable inter, IZone zone);
    // -> new AAddNeighborZone(IZone zone, IZone neighborZone);
    // -> new AAddInteractable(IInteractable inter, IZone zone);
    // -> new AStartTrade(IInteractable trader);
}
