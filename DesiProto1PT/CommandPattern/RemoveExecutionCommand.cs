﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    class RemoveExecutionCommand : IExecutable
    {
        public void AddToCommandStack()
        {
            // dont do anything?
            // or: add it, but remove this AND the previous one on execution
        }

        public void Execute()
        {
            EventAggregator.GetInstance().RemoveLastCommand();
        }
    }
}
