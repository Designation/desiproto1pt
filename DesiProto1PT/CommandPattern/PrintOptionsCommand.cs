﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesiProto1PT.CommandPattern
{
    class PrintOptionsCommand : IExecutable
    {
        private List<PrintCommand> optionPrints = new List<PrintCommand>();
        private List<List<IExecutable>> optionExecutions = new List<List<IExecutable>>();
        private string title;

        public PrintOptionsCommand(string title)
        {
            this.title = title;
        }

        public void AddOption(string option, params IExecutable[] actions)
        {
            this.optionPrints.Add(new PrintCommand(this.optionPrints.Count+1 +". " + option));
            this.optionExecutions.Add(actions.ToList());
        }

        public void Execute()
        {
            new PrintCommand("---- " + this.title + " ----").Execute();
            var counter = 1;

            // print all options
            foreach (var printCommand in this.optionPrints)
            {
                printCommand.Execute();
                counter++;
            }

            // get user input
            var userInput = Console.ReadLine();

            // parse user input (needs protection against non-int inputs) //todo
            int index = Int32.Parse(userInput);

            // reduce by 1 because UI options start at 1, code starts at 0
            index--;

            // invalid input, out of bounds
            if (index < 0 || index >= counter-1)
            {
                new PrintCommand("Invalid input: " + (index-1) + " is out of bounds.").Execute();
                return;
            }

            // execute everything saved for that option
            foreach (var action in this.optionExecutions[index])
            {
                action.Execute();
            }
        }
    }
}
