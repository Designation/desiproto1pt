﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    class ClearCommandStackCommand : IExecutable
    {
        public void Execute()
        {
            EventAggregator.GetInstance().ClearCommandStack();
        }
    }
}
