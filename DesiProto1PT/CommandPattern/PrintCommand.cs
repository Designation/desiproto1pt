﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT.CommandPattern
{
    class PrintCommand : IExecutable
    {
        private String printText;
        private EventAggregator ea;

        public PrintCommand(String printText)
        {
            this.printText = printText;
        }

        public void Execute()
        {
            Console.Write(this.printText + "\n");
        }
    }
}
