﻿using System;

namespace DesiProto1PT.CommandPattern
{
    class UpdateObjectiveCommand : IExecutable
    {
        private String newObjective;
        private Player player;

        public UpdateObjectiveCommand(String newObjective)
        {
            this.newObjective = newObjective;
            this.player = null;
        }

        public void Execute()
        {
            player.CurrentObjective = this.newObjective;
        }
    }
}
