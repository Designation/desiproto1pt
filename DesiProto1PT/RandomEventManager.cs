﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesiProto1PT
{
    class RandomEventManager
    {
        // manages all of the games randomEvents: creation, saving, rolling, execution

        public List<IRandomEvent> EventList { get; set; }

        internal void Init()
        {
            EventAggregator.GetInstance().Subscribe(this);
            this.GenerateRandomEvents();
        }

        private void GenerateRandomEvents()
        {
            // there are no randomEvents yet
        }

        private void RegisterRandomEvent(IRandomEvent newEvent)
        {
            this.EventList.Add(newEvent);
        }

        public void RollRandomEvents()
        {
            // todo
        }
    }
}
